import Cocoa

///
/// The synchronization indicator is controlled by observing the
/// CloudNotesService.
///
/// It is not controlled by the 'synchronize' action of the synchronize button
/// or the synchronize menu entry.
///
extension ViewController {
	
	// MARK: - Observing
	
	///
	/// Adds self as an observer to control the synchronize button.
	///
	func addObserverForSynchronizeButton() {
		notificationCenter.addObserver(
			self,
			selector: #selector(disableSynchronizeButton),
			name: Notification.Name.CloudeNotesService.beginRequests,
			object: nil)
		notificationCenter.addObserver(
			self,
			selector: #selector(enableSynchronizeButton),
			name: Notification.Name.CloudeNotesService.finishRequests,
			object: nil)
	}
	
	///
	/// Removes self as an observer to control the synchronize button.
	///
	func removeObserverForSynchronizeButton() {
		notificationCenter.removeObserver(
			self,
			name: Notification.Name.CloudeNotesService.beginRequests,
			object: nil)
		notificationCenter.removeObserver(
			self,
			name: Notification.Name.CloudeNotesService.finishRequests,
			object: nil)
	}
	
	// MARK: - Control
	
	///
	/// Disables the synchronize button.
	///
	@objc
	private func disableSynchronizeButton() {
		synchronizeButton.isEnabled = false
	}
	
	///
	/// Enables the synchronize button.
	///
	@objc
	private func enableSynchronizeButton() {
		synchronizeButton.isEnabled = true
	}
}
