import Cocoa

///
/// An extension to control the status indicator by observing the
/// CloudNotesService.
///
extension WindowController {
	
	// MARK: - Observing
	
	///
	/// Adds self as an observers to control the status indicator.
	///
	func addObserverForStatusIndicator() {
		notificationCenter.addObserver(
			self,
			selector: #selector(indicateStatusOk(notification:)),
			name: Notification.Name.CloudeNotesService.requestSucceeded,
			object: nil)
		notificationCenter.addObserver(
			self,
			selector: #selector(indicateStatusError(notification:)),
			name: Notification.Name.CloudeNotesService.requestFailed,
			object: nil)
	}
	
	///
	/// Removes self as an observers to control the status indicator.
	///
	func removeObserverForStatusIndicator() {
		notificationCenter.removeObserver(
			self,
			name: Notification.Name.CloudeNotesService.requestSucceeded,
			object: nil)
		notificationCenter.removeObserver(
			self,
			name: Notification.Name.CloudeNotesService.requestFailed,
			object: nil)
	}
	
	// MARK: - Control
	
	///
	/// Displays a status error with a message as a tooltip.
	///
	@objc
	private func indicateStatusError(notification: NSNotification) {
		if let errorMessage = notification.userInfo?[Notification.UserInfo.CloudNoteService.errorMessage] as? String {
			statusIndicator.toolTip = errorMessage
		}
		
		statusIndicator.isHidden = false
	}
	
	///
	/// Removes the 'status error display' and its message.
	///
	@objc
	private func indicateStatusOk(notification: NSNotification) {
		statusIndicator.toolTip = nil
		statusIndicator.isHidden = true
	}
}
