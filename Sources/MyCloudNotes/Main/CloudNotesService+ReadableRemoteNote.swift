import Foundation

extension CloudNotesService {
	
	///
	/// An adapter to use a JSON-dictionary like a readable remote note .
	///
	class ReadableRemoteNote {
		
		///
		/// The JSON-dictionary.
		///
		private let json: [String:Any]
		
		///
		/// Return the server id.
		///
		var id: NSNumber? { return json.value(forKey: CloudNotesService.JsonKey.id) }
		
		///
		/// Return the title.
		///
		var title: String? { return json.value(forKey: CloudNotesService.JsonKey.title) }
		
		///
		/// Return the content.
		///
		var content: String? { return json.value(forKey: CloudNotesService.JsonKey.content) }
		
		///
		/// Return the category.
		///
		var category: String? { return json.value(forKey: CloudNotesService.JsonKey.category) }
		
		///
		/// Return if favorite.
		///
		var favorite: Bool? { return json.value(forKey: CloudNotesService.JsonKey.favorite) }
		
		///
		/// Return modified.
		///
		var modified: Date? {
			guard let number: NSNumber = json.value(forKey: CloudNotesService.JsonKey.modified) else {
				return nil
			}
			
			return Date(timeIntervalSince1970: number.doubleValue)
		}
		
		// MARK: -
		
		///
		/// Creates a new instance to adapt a JSON-dictionary.
		///
		init(_ aJson: [String:Any]) {
			json = aJson
		}
	}
}
