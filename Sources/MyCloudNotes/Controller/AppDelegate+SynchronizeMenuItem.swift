///
/// Extension to bind the synchronization activity' to the 'state of the
/// synchronize menu item'.
///
extension AppDelegate {
	
	// MARK: - Observing
	
	///
	/// Adds self as an observer to control the 'synchronize' menu item.
	///
	func addObserverForSynchronizeMenuItem() {
		notificationCenter.addObserver(
			self,
			selector: #selector(disableSynchronizeMenuItem),
			name: Notification.Name.CloudeNotesService.beginRequests,
			object: nil)
		notificationCenter.addObserver(
			self,
			selector: #selector(enableSynchronizationMenuItem),
			name: Notification.Name.CloudeNotesService.finishRequests,
			object: nil)
	}
	
	///
	/// Removes self as an observer to control the 'synchronize' menu item.
	///
	func removeObserverForSynchronizeMenuItem() {
		notificationCenter.removeObserver(
			self,
			name: Notification.Name.CloudeNotesService.beginRequests,
			object: nil)
		notificationCenter.removeObserver(
			self,
			name: Notification.Name.CloudeNotesService.finishRequests,
			object: nil)
	}
	
	// MARK: - Control
	
	///
	/// Disables the 'synchronize' menu item.
	///
	@objc
	private func disableSynchronizeMenuItem() {
		synchronizationMenuItem.isEnabled = false
	}
	
	///
	/// Enables the 'synchronize' menu item.
	///
	@objc
	private func enableSynchronizationMenuItem() {
		synchronizationMenuItem.isEnabled = true
	}
}
