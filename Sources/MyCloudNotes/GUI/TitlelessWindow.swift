import Cocoa

///
/// A window without a title.
///
public class TitlelessWindow : NSWindow {
	
	///
	/// Hides the title.
	///
	public override init(contentRect aContentRect: NSRect, styleMask aStyle: NSWindow.StyleMask, backing aBackingStoreType: NSWindow.BackingStoreType, defer aFlag: Bool) {
		super.init(contentRect: aContentRect, styleMask: aStyle, backing: aBackingStoreType, defer: aFlag)
		
		titleVisibility = .hidden
	}
}
