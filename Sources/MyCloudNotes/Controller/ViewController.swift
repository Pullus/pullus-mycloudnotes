import Cocoa
import Marklight
import os.log

///
/// The View Controller of the main view.
///
class ViewController: NSViewController {
	
	private typealias SELF = ViewController
	
	@IBOutlet weak var notesTable: NSTableView!
	@IBOutlet weak var scroller1: Scroller!
	@IBOutlet weak var scroller2: Scroller!
	
	// MARK: - Application
	
	///
	/// The main application.
	///
	private var myCloudNotes: MyCloudNotes { return MyCloudNotes.shared }
	
	// MARK: - Notifications
	
	///
	/// Notification Center.
	///
	let notificationCenter = NotificationCenter.default
	
	// MARK: - Core Data
	
	///
	/// The Core Data Context.
	///
	/// HINT: The 'Notes Controller' binds this context.
	///
	@objc
	public let context = MyCloudNotes.shared.context
	
	// MARK: - Outlets
	
	///
	/// The 'Notes Controller'.
	///
	@IBOutlet
	var notesController: NSArrayController!
	
	///
	/// Displays the content of a note.
	///
	@IBOutlet
	var noteText: NSTextView!
	
	///
	/// Button to trigger the synchronization.
	///
	@IBOutlet
	weak var synchronizeButton: NSButton!
		
	// MARK: -
	
	///
	/// Holds the shared objects for GUI components and bindings.
	///
	@objc dynamic
	var sharedState = SharedGUIState.standard
	
	///
	/// The TextStorage of noteText.
	///
	/// Used to hold a default TextStorage or a Marklight-TextStorage.
	///
	/// HINT: It seems that a TextView only keeps a weak reference, so the
	///       TextStorage is keept her.
	///
	var textStorage = NSTextStorage()
	
	// MARK: -
	
	///
	/// Remove all observers
	///
	deinit {
		removeObserverForVisualisation()
		removeObserverForSynchronizeButton()
		removeObserverForFocusOnInsertedNote()
	}
	
	// MARK: - NSViewController
	
	override func viewDidLoad() {
		super.viewDidLoad()

		// Initial sort by 'modified'
		// TODO: Update order after changing a note
		notesController.sortDescriptors = [NSSortDescriptor(key: "modified", ascending: false)]

		setUpGUI()
		
		addObserverForVisualisation()
		addObserverForSynchronizeButton()
		addObserverForFocusOnInsertedNote()
	}
	
	///
	/// Sets the insets of the text container of a Note.
	///
	/// HINT: Check if this can be done with the interface builder. Setting
	///       manually 'left', 'right', ... of the 'ClipView Content insets'
	///       doesn't work.
	///
	private func setUpGUI() {
		noteText.textContainerInset = NSSize(width: 20, height: 20)
		
		let color = notesTable.backgroundColor
		scroller1.backgroundColor = color
		scroller2.backgroundColor = color
	}

	// MARK: - Actions
	
	///
	/// Synchronize the notes with the server.
	///
	@IBAction
	func synchronizeNotes(_ sender: Any?) {
		myCloudNotes.synchronizeNotes()
	}
}
