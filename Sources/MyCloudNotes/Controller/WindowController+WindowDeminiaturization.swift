import Cocoa

///
/// An extension to control the deminiaturization of the main window by
/// observing the CloudNotesService.
///
extension WindowController {
	
	// MARK: - Observing
	
	///
	/// Adds self as an observer to control the deminiaturization of the main
	/// window.
	///
	/// If the application becomes active the main window is deminiaturized.
	///
	func addObserverForDeminiaturization() {
		notificationCenter.addObserver(
			self,
			selector: #selector(deminiaturizeWindow),
			name: NSApplication.didBecomeActiveNotification,
			object: nil)
	}
	
	///
	/// Removes self as an observer to control the deminiaturize of the main
	/// window.
	///
	func removeObserverForDeminiaturization() {
		notificationCenter.removeObserver(
			self,
			name: NSApplication.didBecomeActiveNotification,
			object: nil)
	}
	
	// MARK: - Control
	
	///
	/// Deminiaturizes the window.
	///
	@objc
	private func deminiaturizeWindow(_ notification: Notification) {
		if let window = window, window.isMiniaturized {
			window.deminiaturize(self)
		}
	}
}
