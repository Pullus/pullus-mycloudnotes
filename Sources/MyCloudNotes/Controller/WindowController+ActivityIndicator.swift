import Cocoa

///
/// An extension to control the activity indicator by observing the
/// CloudeNoteService.
///
extension WindowController {
	
	// MARK: - Observing
	
	///
	/// Adds self as an observers to control the activity indicator.
	///
	func addObserverForActivityIndicator() {
		notificationCenter.addObserver(
			self,
			selector: #selector(startActivityIndicator),
			name: Notification.Name.CloudeNotesService.beginRequests,
			object: nil)
		notificationCenter.addObserver(
			self,
			selector: #selector(stopActivityIndicator),
			name: Notification.Name.CloudeNotesService.finishRequests,
			object: nil)
	}
	
	///
	/// Removes self as an observer to control the activity indicator.
	///
	func removeObserverForActivityIndicator() {
		notificationCenter.removeObserver(
			self,
			name: Notification.Name.CloudeNotesService.beginRequests,
			object: nil)
		notificationCenter.removeObserver(
			self,
			name: Notification.Name.CloudeNotesService.finishRequests,
			object: nil)
	}
	
	// MARK: - Control
	
	///
	/// Starts the animation of the activity indicator.
	///
	@objc
	private func startActivityIndicator() {
		activityIndicator.startAnimation(nil)
	}
	
	///
	/// Stops the animation of the activity indicator.
	///
	@objc
	private func stopActivityIndicator() {
		activityIndicator.stopAnimation(nil)
	}
}
