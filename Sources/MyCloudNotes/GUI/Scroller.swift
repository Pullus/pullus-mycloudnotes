import Cocoa
import os.log

///
/// A NSScroller with a custom background color.
///
@objc(Scroller)
public class Scroller : NSScroller {
	
	///
	/// The background color used for the 'slot'.
	///
	@objc dynamic
	var backgroundColor: NSColor?
	
	public override func draw(_ dirtyRect: NSRect) {
		if let color = backgroundColor {
			color.setFill()
			dirtyRect.fill()
		}
		drawKnob()
	}
}
