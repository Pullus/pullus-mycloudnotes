///
/// A collection of objects to be shared between Cocoa components.
///
@objc(SharedGUIState)
class SharedGUIState : NSObject {
	
	///
	/// The global singleton.
	///
	static let standard = SharedGUIState()
	
	// MARK: - Shared objects
	
	///
	/// The predicate to filter Notes.
	///
	/// Shared by the 'Notes Controller' (NSArrayContoller) of the window and
	/// the view.
	///
	@objc dynamic
	var filterPredicate: NSPredicate?
	
	///
	/// The indexes of selected Notes to keep the selection synchronized.
	///
	/// Shared by the 'Notes Controller' (NSArrayContoller) of the window and
	/// the view.
	///
	@objc dynamic
	var selectionIndexes: NSIndexSet?
	
	///
	/// The sort descriptor of the Notes to keep the order synchronized.
	///
	/// Shared by the 'Notes Controller' (NSArrayContoller) of the window and
	/// the view.
	///
	/// This is necessary for correct objects from
	/// NSArrayController.selectedObjects.
	///
	@objc dynamic
	var sortDescriptors: [NSSortDescriptor]?
}
