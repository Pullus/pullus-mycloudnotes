import Cocoa

///
/// An extension to focus on the text view of a manually inserted note.
///
extension ViewController {

	// MARK: Observing
	
	///
	/// Adds self as an observer to control the focus of inserted notes.
	///
	func addObserverForFocusOnInsertedNote() {
		notificationCenter.addObserver(
			self,
			selector: #selector(ViewController.focusOnTextOfInsertedNote),
			name: NSNotification.Name.NSManagedObjectContextObjectsDidChange,
			object: context)
	}
	
	///
	/// Removes self as an observer to control the focus of inserted notes.
	///
	func removeObserverForFocusOnInsertedNote() {
		notificationCenter.removeObserver(
			self,
			name: NSNotification.Name.NSManagedObjectContextObjectsDidChange,
			object: context)
	}
	
	// MARK: - Control
	
	///
	/// Select a manually inserted note and focus on the text view of the note.
	///
	@objc
	func focusOnTextOfInsertedNote(notification: NSNotification) {
		guard let userInfo = notification.userInfo else { return }
		
		// Select the inserted note.
		//
		// HINT: Within Interface Builder the option "Select inserted objects"
		//       is set but but it doesn't work. New objects are not selected.
		//       Maybe this is due to the usage of multiple NSArrayController
		//       or the firstPredicate.
		if  let insertedObjects = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject>,
			let note = (insertedObjects.flatMap { $0 as? Note }.filter { $0.hasChangedAfterSynchronization }.first) {
			notesController.setSelectedObjects([note])
		}
		
		// Set focus to the NSTextView of the selected note
		view.window?.makeFirstResponder(noteText)
	}
}
