import Cocoa

class WindowController: NSWindowController {
	
	// MARK: - Outlets
	
	///
	/// Status indicator of the CloudNotesService.
	///
	@IBOutlet weak var statusIndicator: NSImageView!
	
	///
	/// Spinner to indicate a running synchronization.
	///
	@IBOutlet weak var activityIndicator: NSProgressIndicator!
	
	///
	/// Search field for notes.
	///
	@IBOutlet
	weak var searchField: NSSearchField!
	
	// MARK: - Notifications
	
	///
	/// Notification Center.
	///
	let notificationCenter = NotificationCenter.default
	
	// MARK: - CoreData
	
	///
	/// The Core Data Context.
	///
	/// HINT: The 'Notes Controller' binds this context.
	///
	@objc
	public let context = MyCloudNotes.shared.context
	
	// MARK: -
	
	///
	/// Holds the shared objects for GUI components and bindings.
	///
	@objc dynamic
	var sharedState = SharedGUIState.standard
	
	// MARK: -
	
	///
	/// Remove observer.
	///
	deinit {
		removeObserverForStatusIndicator()
		removeObserverForActivityIndicator()
		removeObserverForDeminiaturization()
	}
	
	// MARK: - NSWindowController
	
	///
	/// Adds an observer to control auto deminiaturize of main window if
	/// application becomes active.
	///
	override func windowDidLoad() {
		addObserverForDeminiaturization()
		addObserverForActivityIndicator()
		addObserverForStatusIndicator()
	}
}
